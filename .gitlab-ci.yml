stages:
  - build
  - test
  - deploy staging
  - deploy production
  - production tests
  - cache

#Caches can be set at the job level or globally; set globally here
cache: #setting the cache optimizes the pipeline; you need to specify a 'key' and a 'path'
    key: ${CI_COMMIT_REF_SLUG} #key can be a string, but using this env variable uses whatever the current branch is 
    paths:
      - node_modules/ #items within this directory will be downloaded later and used
    policy: pull #sets a policy for the cache, pull only downloads, with this set any changes made will not be pushed to the cache

variables:
  STAGING_DOMAIN: glowdisk-staging.surge.sh 
  PRODUCTION_DOMAIN: glowdisk.surge.sh

build website:
  stage: build
  image: node:10 #setting the 'image' changes the image used from whatever the default is; an image with the stack required should be used. in this case npm
  script:
    - echo $CI_COMMIT_SHORT_SHA #displays the commit revision number
    - npm install
    - npm install -g gatsby-cli
    - gatsby build 
    - sed -i "s/%%VERSION%%/$CI_COMMIT_SHORT_SHA/" ./public/index.html #adds the commit as a version number on the home page, 'CI_COMMIT_SHA' is a predefined Gitlab variable
  artifacts:
    paths:
      - ./public 
  except:
    - schedules
      
test artifact:
  cache: {} #overrides the global cache; empty set = do not use, also saves time, avoids pulling the cache everytime, not needed for this job
  stage: test
  image: alpine
  script: #testing for a failure and success to prove test accuracy 
    - grep -q "Gatsby" ./public/index.html
    #- grep "XXXXXXX" ./public/index.html
  except:
    - schedules
    
test website:
  stage: test
  image: node:10
  script:
    - npm install
    - npm install -g gatsby-cli
    - gatsby serve & #starts the server with the production build. The '&' runs the process in the background
    - sleep 5
    - curl "http://localhost:9000" | tac | tac | grep -q "Gatsby" #tac command needed to resolve grep writing body error
  except:
    - schedules

deploy staging:
  stage: deploy staging
  environment: #sets the environment for the job
    name: staging
    url: http://$STAGING_DOMAIN
  image: node:10
  script:
    - npm install --global surge
    - surge --project ./public --domain $STAGING_DOMAIN
  except:
    - schedules
  only:
    - master #sets it so that this job only runs when on the master branch

deploy production:
  stage: deploy production
  environment:
    name: production
    url: http://$PRODUCTION_DOMAIN
  image: node:10
  script:
    - npm install --global surge
    - surge --project ./public --domain $PRODUCTION_DOMAIN
  except:
    - schedules
  when: manual #this job is only triggered when kicked off from the Gitlab interface
  allow_failure: false
  only:
    - master
  
prodution tests:
  cache: {}
  image: alpine
  stage: production tests
  script:
    - apk add --no-cache curl
    - curl -s "https://$PRODUCTION_DOMAIN" | grep -q "Hi people"
    - curl -s "https://$PRODUCTION_DOMAIN" | grep -q "$CI_COMMIT_SHORT_SHA" #predefined Gitlab variable
  except:
    - schedules
  only:
    - master
    
update cache:
  stage: cache
  image: node:10
  script:
    - npm install
  cache:
    key: ${CI_COMMIT_REF_SLUG} #key can be a string, but using this env variable uses whatever the current branch is; jobs of each branch always use the same cache
    paths:
      - node_modules/ #items within this directory will be downloaded later and used
    policy: push #job level policy to push cache only
  only:
    - schedules #this job is run only when pipeline is triggered by a schedule
    